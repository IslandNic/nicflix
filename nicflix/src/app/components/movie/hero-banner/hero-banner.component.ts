import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '@interface/movie.interface';
import { genreType } from '@models/movie.model'

@Component({
  selector: 'app-hero-banner',
  templateUrl: './hero-banner.component.html',
  styleUrls: ['./hero-banner.component.scss']
})

export class HeroBannerComponent implements OnInit {
  @Input() heroMovie: Movie;

  constructor() { }

  ngOnInit() {

    //set dummy movie
    this.heroMovie = {
      id: 2,
      key: "we-are-the-millers",
      name: "We're the Millers",
      description: "A veteran pot dealer creates a fake family as part of his plan to move a huge shipment of weed into the U.S. from Mexico.",
      genres: [genreType.adventure, genreType.comedy, genreType.crime],
      rate: "7.0",
      length: "1hr 50mins",
      img: "we-are-the-millers.jpg"
    }
  }

}
