import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegularScrollerComponent } from './regular-scroller.component';

describe('RegularScrollerComponent', () => {
  let component: RegularScrollerComponent;
  let fixture: ComponentFixture<RegularScrollerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegularScrollerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegularScrollerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
