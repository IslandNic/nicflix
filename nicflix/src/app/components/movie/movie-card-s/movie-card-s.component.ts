import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '@interface/movie.interface';

@Component({
  selector: 'app-movie-card-s',
  templateUrl: './movie-card-s.component.html',
  styleUrls: ['./movie-card-s.component.scss']
})
export class MovieCardSComponent implements OnInit {

  @Input() movieItem : Movie;

  constructor() { }

  ngOnInit() {
  }

}
