import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieCardSComponent } from './movie-card-s.component';

describe('MovieCardSComponent', () => {
  let component: MovieCardSComponent;
  let fixture: ComponentFixture<MovieCardSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieCardSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
