import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroBannerComponent } from './hero-banner/hero-banner.component';
import { MovieCardSComponent } from './movie-card-s/movie-card-s.component';
import { RegularScrollerComponent } from './regular-scroller/regular-scroller.component';
import { MovieComponent } from './movie.component';
import { TopMenuComponent } from '../top-menu/top-menu.component';
import { FeedComponent } from './feed/feed.component';
import { SearchComponent } from '../search/search.component';
import { LogoComponent } from '../logo/logo.component';


@NgModule({
  declarations: [
    MovieComponent,
    HeroBannerComponent,
    MovieCardSComponent,
    RegularScrollerComponent,
    TopMenuComponent,
    FeedComponent,
    SearchComponent,
    LogoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MovieModule { }
