import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})


export class FeedComponent implements OnInit {

  feedType = ['Recently Added', 'Trending Now', 'Continue Watching', 'Watch Again' ];
   
  constructor() { }

  ngOnInit() {
  }

}
