import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { Logout } from '../auth/auth.actions';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { userName, isLoggedIn, isLoggedOut } from '../auth/auth.selectors';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  userName$: Observable<string>;
  isLoggedIn$: Observable<boolean>;
  isLoggedOut$: Observable<boolean>;


  constructor(
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit() {

    this.isLoggedIn$ = this.store
      .pipe(
        select(isLoggedIn)
      )

    this.isLoggedOut$ = this.store
      .pipe(
        select(isLoggedOut)
      )

    this.userName$ = this.store
      .pipe(
        select(userName)
      )
  }

  changeProfile() {
    this.store.dispatch(new Logout());
  }
}
