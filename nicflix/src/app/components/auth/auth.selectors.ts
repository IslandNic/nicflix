import { AuthState } from './auth.reducer';
import { createSelector } from "@ngrx/store";

export const selectAuthState = state => state.auth

export const isLoggedIn = createSelector(
    selectAuthState,
    auth => auth.loggedIn
);
export const userName = createSelector(
    selectAuthState,
    auth => auth.user.name 
);
export const isLoggedOut = createSelector(
    isLoggedIn,
    loggedin => !loggedin
);