import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileLauncherComponent } from './profile_launcher/profile-launcher.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AuthService } from './auth.service';
import { AuthGaurd } from './auth.guard';
import { EffectsModule, Actions} from '@ngrx/effects';
import { AuthEffects } from './auth.effects';


@NgModule({
  declarations: [
    ProfileLauncherComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    StoreModule.forFeature('auth', fromAuth.authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ]
})
export class AuthModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        Actions,
        EffectsModule
      ]
    }
  }
}
