import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileLauncherComponent } from './profile-launcher.component';

describe('ProfileLauncherComponent', () => {
  let component: ProfileLauncherComponent;
  let fixture: ComponentFixture<ProfileLauncherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileLauncherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLauncherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
