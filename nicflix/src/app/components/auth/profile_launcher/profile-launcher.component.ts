import { User } from '@models/user.model';
import { AuthService } from './../auth.service';
import { AppState } from './../../../reducers/index';
import { Store, select } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Login, Logout } from '../auth.actions';
import { Router } from '@angular/router';
import { Subscription, noop, Observable} from 'rxjs'
import { tap, map } from 'rxjs/operators';
import { isLoggedIn, isLoggedOut } from '../auth.selectors';



@Component({
    selector: 'app-profile-launcher',
    templateUrl: './profile-launcher.component.html',
    styleUrls: ['./profile-launcher.component.scss']
})
export class ProfileLauncherComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    user: User;
    subscription: Subscription;
    isLoggedIn$: Observable<boolean>;
    isLoggedOut$: Observable<boolean>;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private store: Store<AppState>,
        private router: Router
    ) { }

    ngOnInit() {
        this.buildForm();
        this.isLoggedIn$ = this.store
            .pipe(
            select(isLoggedIn)
    )
    
    this.isLoggedOut$ = this.store
    .pipe(
      select(isLoggedOut)
    )
    }

    get f() { return this.registerForm.controls; }

    buildForm() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.registerForm.valid) {
            try {
                return this.authService.loginUser(this.registerForm.controls['firstName'].value)
                    .pipe(
                        tap(user => {
                            this.store.dispatch(new Login({ user: user }))
                            this.router.navigateByUrl('/dashboard');
                        }))
                    .subscribe();
            } catch (error) {
                alert('Login failed. Try watching as a guest or use the following credentials => First Name: Nicholis and Password: 123456')
            }
        }
    }

    logOut(){
        this.store.dispatch(new Logout());
      }

    guestLogin(){
       return this.authService.loginUser("guest").pipe(
            tap(user => {
                this.store.dispatch(new Login({ user: user }))
                this.router.navigateByUrl('/dashboard');
            }))
        .subscribe();
    }
}
