
import { User } from '@models/user.model';
import { Injectable } from '@angular/core';
import { Observable, noop } from "rxjs";
import { of } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;

  constructor() { }
  // mock backend authentication with profile

  loginUser(name: string): Observable<User> {
    switch (name.toString().toLowerCase()) {
      case 'nicholis':
        return of(this.user = {
          name : "Nicholis",
          email : "nicholis.muller@gmail.com",
          favs : [1, 5, 8],
          startedMovies : [9, 12, 21, 18],
          completedMovies : [3, 5, 9, 21, 16, 13, 17],
          wishList : [],
          focussed: Math.floor(Math.random() * 21)
          // focussed: this.user.startedMovies && this.user.startedMovies.length > 0 ? this.user.startedMovies[Math.floor(Math.random() * this.user.startedMovies.length)] : 1
        })
    
        
        case 'dewi':
        return of(this.user = {    
          name : "Dewi",
          email : "donsie78@gmail.com",
          favs : [3, 4, 9, 4],
          startedMovies : [2, 4, 6, 12, 15],
          completedMovies : [21, 4, 8, 19, 11, 8],
          wishList : [],
          focussed : Math.floor(Math.random() * 21)
          // focussed : this.user.startedMovies && this.user.startedMovies.length > 0 ? this.user.startedMovies[Math.floor(Math.random() * this.user.startedMovies.length)] : 1
        })

        case 'guest':
        return of(this.user = {    
          name : "Guest",
          email : "",
          favs : [],
          startedMovies : [],
          completedMovies : [],
          wishList : [],
          focussed : Math.floor(Math.random() * 21)
        })
        default :
        return undefined;
    }
  }
}
