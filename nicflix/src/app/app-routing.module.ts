import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieComponent } from './components/movie/movie.component';
import { ProfileLauncherComponent } from './components/auth/profile_launcher/profile-launcher.component';
import { AuthGaurd } from './components/auth/auth.guard';

const routes: Routes = [

  {
    path: '',
    component: ProfileLauncherComponent
  },
  {
    path: 'login',
    component: ProfileLauncherComponent
  },
  {
    path: './',
    component: ProfileLauncherComponent
  }, {
    path: 'dashboard',
    component: MovieComponent,
    canActivate: [AuthGaurd]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGaurd]
})
export class AppRoutingModule { }
