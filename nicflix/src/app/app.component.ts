import { Component, OnInit } from '@angular/core';
import { DataService } from './../server/data-service.service';
import { tap, map } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'NicFlix';

    constructor(
        private dataService: DataService
    ) {

    }

    ngOnInit() {
        //get initial data from BE and set state
        this.getMovieData();
        this.getPlatformData();
    }

    getMovieData() {
        return this.dataService.getMovieData().pipe(
            tap(movies => {
                //todo set state with movies
                //this.store.displatch()
                console.log('movies', movies)
            })
        ).subscribe();
    }

    getPlatformData() {
        this.dataService.getPlatformStats().pipe(
            tap(stats => {
                //todo set state with initial platform data
                //this.store.displatch()
                console.log('stats', stats)
            })
        ).subscribe();
    }
}
