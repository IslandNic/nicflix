export interface PlatformStats {
    recentlyAdded : Array<number>,
    trendingNow: Array<number>,
    nicOriginals: Array<number>
}
