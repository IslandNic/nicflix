export interface Movie {
    id: number,
    key: string,
    name: string,
    description: string,
    genres: Array<String>,
    rate: string,
    length: string,
    img: string
}