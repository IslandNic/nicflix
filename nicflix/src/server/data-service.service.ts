import { Movie } from '@interface/movie.interface';

import { Injectable } from '@angular/core';
import { movieData } from './movie.mock-data';
import { platformStats } from './platform-stats-data';
import { Observable, noop } from "rxjs";
import { of } from "rxjs";
import { PlatformStats } from './../app/interfaces/platform-data.interface';

 



@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getPlatformStats(): Observable<PlatformStats> {
    return of(platformStats);
  }

  getMovieData(): Observable<Array<Movie>>{
    return of(movieData)
  }
}
